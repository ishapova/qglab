# Installation

QGLab is a Python package that can be installed using pip. The best (i.e., automatic and clean) way to install the package and its dependencies (`qiskit`, `matlab`, `pylatexenc`) is to use `pip` within a virtual environment.

Create and activate a virtual environment:
```
python3 -m venv /path/to/myqglabenv
source /path/to/myqglabenv/bin/activate
```
Optionally, upgrade `pip` and install `wheel` for a smoother installation experience:
```
python -m pip install -U pip
pip install wheel
```
Clone the git repository to your machine:
```
git clone git@gitlab.com:ishapoval/qglab.git /path/to/myqglab
```
and, finally, install `qglab` (in development mode) and all dependencies:
```
cd /path/to/myqglab
pip install --editable .
```

One can check if the installation went successfully with 
```
python scripts/wormhole.py --help
```

# IBM account

To be able to run noisy simulations and experiments on quantum devices you must have an IBM account. If you don't have one register [here](https://quantum-computing.ibm.com/). If it is not saved to disk save it before running the main script with:
```
from qiskit import IBMQ
IBMQ.save_account(TOKEN, hub=HUB, group=GROUP, project=PROJECT)
```
The main script will then load the credentials and determine available devices based on the type of your account.

# Running

The main script located at `scripts/wormhole.py` can be executed in several modes. Find below a few examples.

Run ideal and noisy simulations choosing a quantum device to mimick interactively:
```
python wormhole.py --points 14 --retrials 5 --dry-run
```
Same as above, but also select the best layout interactively (currently supported only for `ibmq_16_melbourne`):
```
python wormhole.py -p 14 -r 5 --dry-run --layout select
```
Same as above, but also run quantum experiment:
```
python wormhole.py -p 14 -r 5 --layout select
```
Each execution of the script will generate a folder with debug info and run results.


# Copyright notice
QGLab Copyright (c) 2021, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of
any required approvals from the U.S. Dept. of Energy) and University
of California, Berkeley.  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative 
works, and perform publicly and display publicly, and to permit others to do so.

