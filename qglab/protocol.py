from typing import List
from qiskit import QuantumCircuit


class TeleporterFactory(object):
    """Helper class for constructing a holographic teleportation circuit."""
    def __init__(self,
                 n_qubits_per_side: int,
                 message_size: int,
                 x_rotation_transverse_angle: float,
                 zz_rotation_angle: float,
                 z_rotation_angles: List[float],
                 time_steps: int,
                 insert_message_method: str,
                 debug: bool = False):
        """Initialize the helper instance.
        Args:
            n_qubits: number of qubits per one side
            message_size: number of qubits used to encode a message
        """

        print("\nQubits per side: ", n_qubits_per_side)
        self.N = n_qubits_per_side
        self.m = message_size

        self.x_rotation_transverse_angle = x_rotation_transverse_angle
        self.zz_rotation_angle = zz_rotation_angle
        self.z_rotation_angles = z_rotation_angles
        print("Time steps: ", time_steps)
        self.time_steps = time_steps
        assert insert_message_method in [
            'reset', 'swap', 'transfer'
        ], "Requested unknown method for message insertion"
        print("Message insertion method: ", insert_message_method)
        self.insert_message_method = insert_message_method
        #TODO turn it to DEBUG logging
        self.debug = debug

        # Make L and R systems mirror each other across the bipartition
        self.L = list(range(0, n_qubits_per_side))
        self.R = list(reversed(range(n_qubits_per_side,
                                     2 * n_qubits_per_side)))

    def _make_bell_pair(self, i: int, j: int, circ: QuantumCircuit):
        """Assuming qubits i and j are in the |00> state, create bell pair (|00> + |11>)"""
        circ.h(i)
        circ.cnot(i, j)

    def _inf_temp_TFD(self, circ: QuantumCircuit):
        """Prepare bell pairs between qubits on the left and right systems"""
        for _l, _r in zip(self.L, self.R):
            self._make_bell_pair(_l, _r, circ)

    def _U_K(self, theta: float, qubits: List[int], circ: QuantumCircuit):
        """Apply transverse X rotation gate by 2*theta for each qubit in qubits"""
        for q in qubits:
            circ.rx(2 * theta, q)

    def _U_I(self, theta: float, phis: List[float], qubits: List[int],
             circ: QuantumCircuit):
        """Apply ZZ rotation gate by 2*theta and Z rotation by 2*phi for each qubit in qubits"""
        for q, phi_i in zip(qubits, phis):
            circ.rz(2 * phi_i, q)

        for i in range(len(qubits) - 1):
            q1, q2 = qubits[i], qubits[i + 1]
            circ.rzz(2 * theta, q1, q2)

    def _Hamiltonian_step(self,
                          subsystem: List[int],
                          circ: QuantumCircuit,
                          x_rotation_transverse_angle: float,
                          zz_rotation_angle: float,
                          z_rotation_angles: List[float],
                          n_repeat: int,
                          forward: bool = True,
                          transpose: bool = False):
        """
        subsystem is a list of qubit indices to act on
        theta_b, theta_J, h are model parameters of the quantum kick
        forward denotes +time evolution
        transpose denotes L/R subsystems and reverses the order
        """
        if not forward:  # backwards time evolution
            x_rotation_transverse_angle, zz_rotation_angle, z_rotation_angles = \
                -x_rotation_transverse_angle, -zz_rotation_angle, -z_rotation_angles

        for _ in range(n_repeat):
            # One ordering for forwards evolution on L or backwards on R
            if transpose != forward:
                self._U_I(zz_rotation_angle, z_rotation_angles, subsystem,
                          circ)
                self._U_K(x_rotation_transverse_angle, subsystem, circ)
            # Second ordering for backwards evo on L or forwards on R
            else:
                self._U_K(x_rotation_transverse_angle, subsystem, circ)
                self._U_I(zz_rotation_angle, z_rotation_angles, subsystem,
                          circ)

    def _two_sided_coupling(self, coupling_strength: float,
                            circ: QuantumCircuit):
        """couple the two sides with exp(i*coupling_strength*V) on self.N-self.m carrier qubits
        V is given by an average over Z_l Z_r operators (of which there are self.N-self.m)
        Note again the factor of 2 in the exponent since rotations are done by theta/2.
        """
        prefactor = 1. / (self.N - self.m)
        for _l, _r in zip(self.L[self.m:], self.R[self.m:]):
            circ.rzz(2 * coupling_strength * prefactor, _l, _r)

    def circuit(self, interaction_coupling_strength: float) -> QuantumCircuit:
        """Run the teleportation protocol for quantum kicked ising.
        Returns the expectation value Z measured on of the teleported qubit"""

        total_n_qubits = 2 * self.N if self.insert_message_method == 'reset' else 2 * self.N + 1

        circ = QuantumCircuit(
            total_n_qubits,
            1,
            name=f"circuit_g_{interaction_coupling_strength:.4f}")

        self._inf_temp_TFD(circ)
        #circ.barrier()

        # time evolve backward (L)
        self._Hamiltonian_step(self.L,
                               circ,
                               self.x_rotation_transverse_angle,
                               self.zz_rotation_angle,
                               self.z_rotation_angles,
                               self.time_steps,
                               forward=False,
                               transpose=False)
        #circ.barrier()

        if self.insert_message_method == 'swap':
            # swap (L_1 and ancilla)
            circ.swap(self.L[0], 2 * self.N)
        elif self.insert_message_method == 'reset':
            circ.reset(self.L[0])
        else:  # apply the state transfer protocol
            self.L[0] = 2 * self.N

        # time evolve forward (L)
        #circ.barrier()
        self._Hamiltonian_step(self.L,
                               circ,
                               self.x_rotation_transverse_angle,
                               self.zz_rotation_angle,
                               self.z_rotation_angles,
                               self.time_steps,
                               forward=True,
                               transpose=False)

        # two sided coupling
        #circ.barrier()
        self._two_sided_coupling(interaction_coupling_strength, circ)

        # time evolve forward (R)
        #circ.barrier()
        self._Hamiltonian_step(self.R,
                               circ,
                               self.x_rotation_transverse_angle,
                               self.zz_rotation_angle,
                               self.z_rotation_angles,
                               self.time_steps,
                               forward=True,
                               transpose=True)
        #circ.barrier()

        # Measure the message
        circ.measure(self.R[0], 0)

        if self.debug:
            print(circ)

        return circ
