"""This module provides general utilities facilitating the execution of
simulations and quantum experiments"""

import time
import datetime
from typing import List, Dict, Union
from itertools import count
import numpy as np

from qiskit import QuantumCircuit, Aer

from qiskit.opflow import I, Z, AerPauliExpectation, CircuitSampler, StateFn, CircuitStateFn

from qiskit.circuit.measure import Measure
from qiskit.circuit.library import CXGate
from qiskit.providers.ibmq.managed import IBMQJobManager
from qiskit.providers.jobstatus import JobStatus, JOB_FINAL_STATES

#from qiskit.tools.monitor import job_monitor #follows old qiskit spec
from qiskit.providers.ibmq.job import job_monitor  # uses ibm specific functionality

from qiskit.aqua.utils.backend_utils import is_ibmq_provider, is_simulator_backend

from pytket.extensions.qiskit import qiskit_to_tk, tk_to_qiskit
from pytket.extensions.qiskit import IBMQBackend

#from pytket import OpType
from pytket.circuit import Qubit, Node
from pytket.passes import (DecomposeBoxes, FullPeepholeOptimise, CliffordSimp,
                           CXMappingPass, DecomposeSwapsToCXs,
                           RemoveRedundancies)
#DecomposeMultiQubitsIBM, RoutingPass, RebaseIBM, DefaultMappingPass, RebaseCustom)
from pytket.routing import Placement, NoiseAwarePlacement, GraphPlacement, LinePlacement, route
from pytket.predicates import ConnectivityPredicate  #, CompilationUnit


class LayoutAdapter:
    """Adapter class to facilitate manipulation of Qiskit and tket layouts"""

    _idx = count(0)

    def __init__(self, layout: Union[List[int], Dict[Qubit, Node]], sdk: str):

        assert isinstance(layout,
                          (list, dict)), f"Unknown layout type {type(layout)}"

        self.idx = next(self._idx)

        assert sdk in ['qiskit',
                       'tket'], "Requested unknown SDK as destination"

        self.sdk = sdk

        if isinstance(layout, list):
            self.layout_flat = layout
            self.layout_tket = {
                Qubit(i): Node(j)
                for i, j in enumerate(layout)
            }
        else:  #isinstance(layout, dict):
            self.layout_flat = [
                layout[k].index[0]
                for k in sorted(layout.keys(), key=lambda q: q.index)
            ]
            self.layout_tket = layout

    def get(self):
        "Get layout represntation appropriate for "
        return self.layout_flat if self.sdk == 'qiskit' else self.layout_tket

    def __str__(self):
        return str(self.layout_flat) if self.sdk == 'qiskit' else str(
            self.layout_tket).replace('ode', '')


def timeit(func):
    """Timing decorator"""
    def wrapper(*args, **kwargs):
        start = time.perf_counter()
        rval = func(*args, **kwargs)
        end = time.perf_counter()
        print(
            f"   ... '{func.__name__}' wall-clock time is {((end - start) / 60.):.2f}m"
        )
        return rval

    return wrapper


def tabulate(rows, headers):
    """Tabulate rows (2D list) with headers (1D list)"""

    n_columns = len(headers)

    for row in rows:
        assert len(row) == n_columns, "Number of columns is not homogeneous"

    max_indent_by_column = [len(str(h)) for h in headers]
    for row in rows:
        for i, elem in enumerate(row):
            elem_length = len(str(elem))
            if elem_length > max_indent_by_column[i]:
                max_indent_by_column[i] = elem_length

    formated_row = "  "
    for indt in max_indent_by_column:
        format_instr = f"{{:<{indt+3}}}"
        formated_row += f"{format_instr}"

    print('\n', formated_row.format(*headers), sep='')

    for row in rows:
        print(formated_row.format(*row))


def find_measured_qubits(circ):
    """Find and return the indices of all Measure gates in a circuit"""

    return [
        qb_list[0].index for instr, qb_list, cb_list in circ.data
        if isinstance(instr, Measure)
    ]


def count_cx(circ, qubit_idx):
    """Count the number of CNOTs in a circuit on a given qubit"""

    _count = 0

    for instr, qb_list, _ in circ.data:
        if isinstance(instr, CXGate) and qubit_idx in [
                qb_list[0].index, qb_list[1].index
        ]:
            _count += 1

    return _count


def get_tket_layouts(qiskit_circ: QuantumCircuit, device_name: str, provider,
                     layout_method: str):
    "Get best placements according to given layout method"

    assert layout_method in [
        'noise_adaptive', 'graph_based', 'linear'
    ], f"Placement method {layout_method} is not supported by tket"

    tket_circ = qiskit_to_tk(qiskit_circ)
    ibmq_b = IBMQBackend(device_name,
                         hub=provider.credentials.hub,
                         group=provider.credentials.group,
                         project=provider.credentials.project)

    # optimize circuit
    CliffordSimp().apply(tket_circ)
    CliffordSimp().apply(tket_circ)
    # keep this sequence as a backup, but for Paris it had no effect
    FullPeepholeOptimise().apply(tket_circ)

    # this alternative sequence yielded unrouted 18 CNOTs, but routing this
    # more connected graph requires more CNOTs in the end (at least for IBM Q Paris)
    # RemoveRedundancies().apply(tket_circ)
    # DecomposeMultiQubitsIBM().apply(tket_circ)
    # CliffordSimp().apply(tket_circ)

    if layout_method == 'noise_adaptive':
        placement = NoiseAwarePlacement(ibmq_b.device)
    elif layout_method == 'graph_based':
        placement = GraphPlacement(ibmq_b.device)
        #placement.modify_config(depth_limit=150)
    else:  #'linear'
        placement = LinePlacement(ibmq_b.device)

    return [
        LayoutAdapter(lt, sdk='tket')
        for lt in Placement.get_placement_maps(placement, tket_circ)
    ]


def tket_transpile(qiskit_circ: QuantumCircuit,
                   device_name: str,
                   provider,
                   layout_method: str,
                   initial_layout: LayoutAdapter = None):
    """Compile and optimize a Qiskit circuit with tket. Accepts and returns a Qiskit circuit."""

    assert layout_method in [
        'noise_adaptive', 'graph_based', 'linear'
    ], f"Placement method {layout_method} is not supported by tket"

    tket_circ = qiskit_to_tk(qiskit_circ)
    #cu = CompilationUnit(tket_circ)

    ibmq_b = IBMQBackend(device_name,
                         hub=provider.credentials.hub,
                         group=provider.credentials.group,
                         project=provider.credentials.project)
    connected = ConnectivityPredicate(ibmq_b.device)

    # compile with default sequence of passes
    #ibmq_b.compile_circuit(tket_circ, optimisation_level=2)

    # ============================== optimize ===========================
    DecomposeBoxes().apply(tket_circ)

    # optimize circuit
    CliffordSimp().apply(tket_circ)
    CliffordSimp().apply(tket_circ)
    # keep this sequence as a backup, but for Paris it had no effect
    # (CliffordSimp, RemoveRedundancies, CommuteThroughMultis, KAKDecomposition,
    # and EulerAngleReduction), won't generally preserve gateset, connectivity, etc
    FullPeepholeOptimise().apply(tket_circ)

    # this alternative sequence yielded unrouted 18 CNOTs, but routing this
    # more connected graph requires more CNOTs in the end (at least for IBM Q Paris)
    # RemoveRedundancies().apply(tket_circ)
    # DecomposeMultiQubitsIBM().apply(tket_circ)
    # CliffordSimp().apply(tket_circ)

    # ============== Placement and routing ===================================
    if initial_layout is None:

        if layout_method == 'noise_adaptive':
            placement = NoiseAwarePlacement(ibmq_b.device)
        elif layout_method == 'graph_based':
            placement = GraphPlacement(ibmq_b.device)
            #placement.modify_config(depth_limit=150)
        else:  #'linear'
            placement = LinePlacement(ibmq_b.device)

        # Performs placement and routing. Combines GraphPlacement and
        # solves any remaining invalid multi-qubit operations (with RoutingPass?)
        #DefaultMappingPass(ibmq_b.device).apply(tket_circ)

        # More advanced placement and routing. Adds decomposition of the introduced
        # OpType.SWAP and OpType.BRIDGE gates into elementary OpType.CX gates
        CXMappingPass(ibmq_b.device, placement).apply(tket_circ)
        RemoveRedundancies().apply(tket_circ)
        routed_tket_circ = tket_circ
    else:
        assert isinstance(initial_layout, LayoutAdapter)

        # place the circuit
        Placement.place_with_map(tket_circ, initial_layout.get())

        # route the circuit
        #RoutingPass(ibmq_b.device).apply(tket_circ)
        #basic_parameters = dict(bridge_lookahead=2,#bridge_interactions=4,swap_lookahead=50)
        routed_tket_circ = route(tket_circ,
                                 ibmq_b.device)  #, **basic_parameters)

        # decompose all SWAP and BRIDGE gates to CX gates and perform
        # light clean up (CX cancelation to I)
        DecomposeSwapsToCXs(ibmq_b.device).apply(routed_tket_circ)

    RemoveRedundancies().apply(routed_tket_circ)

    # verify connectivity satisfies the device topology
    assert connected.verify(routed_tket_circ), "Imperfect placement to device"

    # this return a Qiskit circuit based on cx and U* gates
    return tk_to_qiskit(routed_tket_circ)


def calculate_and_save_qfast_unitary(circuit: QuantumCircuit, fname: str):
    """Calculate and save the unitary for supplied circuit and save it"""

    #TODO move this up and make qfast a proper dependency of QGLab if it proves to be useful
    from qfast import perm

    prep_circ = circuit.remove_final_measurements(inplace=False)
    prep_circ.save_unitary()

    backend = Aer.get_backend('aer_simulator')
    unitary = backend.run(prep_circ).result().get_unitary(prep_circ)

    # permute the unitary to comply with QFAST conventions
    num_qubits = int(np.log2(len(unitary)))
    qubit_order = tuple(reversed(range(num_qubits)))
    P = perm.calc_permutation_matrix(num_qubits, qubit_order)
    perm_unitary = P @ unitary @ P.T

    np.savetxt(fname, perm_unitary)


class WormholeRunner:
    """Helper class for launching simulations and quantum experiments."""
    def __init__(self, circuits: List[QuantumCircuit], shots: int,
                 retrials: int, if_dry_run: bool):

        assert len(circuits) != 0, "No circuits provided"
        self.circuits = circuits

        self.shots = shots
        self.retrials = retrials

        self.dry_run = if_dry_run

    @timeit
    def exact_expectation(self, backend):
        """Calculate expectation exactly"""

        assert is_simulator_backend(backend), "Backend is not a simulator"

        expectations = []

        for crct in self.circuits:
            circ_measureless = crct.remove_final_measurements(inplace=False)
            psi = CircuitStateFn(circ_measureless)
            # TODO generalize on the number and position of message qubits
            # figure out the number of qubits per two sides, minus one message qubit
            n_qubits = len(crct.qubits) - 1
            # Measure on the second to last qubit
            op = StateFn(I ^ Z ^ (I ^
                                  (n_qubits - 1)), is_measurement=True) @ psi
            expectation = AerPauliExpectation().convert(op)
            sampler = CircuitSampler(backend=backend).convert(expectation)

            Z_1R = sampler.eval().real

            expectations.append(Z_1R)

        return expectations

    @timeit
    def run(self, transpiled_circuits, backend, async_submit=False, **kwargs):
        """Simulate or run quantum experiments"""

        assert is_simulator_backend(backend) or is_ibmq_provider(
            backend
        ), f"Runner is unaware of how to run on '{type(backend)}' backend"

        ensemble_transpiled_circuits = [
            c.copy(self._add_exp_suffix(c.name, idx))
            for c in transpiled_circuits
            for idx in range(1, self.retrials + 1)
        ]

        results = None

        # Shot-based simulation
        if is_simulator_backend(backend):
            print("   ... running on", backend)

            job = backend.run(
                ensemble_transpiled_circuits,
                shots=self.shots,
                max_parallel_threads=0,
                max_parallel_experiments=0,
                max_parallel_shots=0,
                #optimize_noise_threshold=5, fusion_threshold=6,
                #optimize_ideal_threshold=5, fusion_max_qubit=8,
                **kwargs)

            results = job.result()

        # perform quantum experiment
        if is_ibmq_provider(backend) and not self.dry_run:

            job_manager = IBMQJobManager()
            print("   ... submitting jobs to", backend)
            dt = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')

            job_set = job_manager.run(experiments=ensemble_transpiled_circuits,
                                      backend=backend,
                                      name=f"wormhole_experiment_{dt}",
                                      shots=self.shots)

            time.sleep(5)
            print(job_set.report())

            print("\n   ... verifying job submissions")
            try:
                jobs = job_set.jobs()
            except KeyboardInterrupt:
                # try again
                jobs = job_set.jobs()

            if jobs.count(None) or len(jobs) == 0:
                print(
                    "Some or all of {} job submissions failed, cancelling entire job set session"
                    .format(len(jobs)))
                self._cancel(jobs)
            elif not async_submit:
                try:
                    self._monitor_jobs(jobs)

                    # fetch error messages
                    errors = job_set.error_messages()
                    if errors is not None:
                        print("\n Errors occurred:")
                        print(errors)

                    results = self._fetch_results(job_set)

                except KeyboardInterrupt:
                    self._cancel(jobs)

        return results

    def expectations(self, results, readout_fltr=None):
        """Calculate expectations and, if a partial readout error filter
         is provided, mitigate readout errors"""

        if results is None:
            return None

        expectations = []
        cached_filters = {}

        def mylambda(name):
            return lambda x: x.header.name == name

        for crct in self.circuits:
            z1r_set = []
            for indx in range(1, self.retrials + 1):
                exp_name = self._add_exp_suffix(crct.name, indx)
                counts = results.get_counts(exp_name)

                if readout_fltr is not None:
                    target_results = list(
                        filter(mylambda(exp_name), results.results))

                    assert len(
                        target_results
                    ) == 1, f"Found more than one result for circuit name {exp_name}"

                    meas_qubits = target_results[0].header.metadata[
                        'meas_qubits']
                    meas_qubits_str = str(meas_qubits)
                    if meas_qubits_str not in cached_filters:
                        cached_filters[meas_qubits_str] = readout_fltr(
                            meas_qubits)

                    # mitigate readout errors
                    counts = cached_filters[meas_qubits_str].apply(counts)

                if '0' not in counts.keys() or '1' not in counts.keys():
                    print(
                        f"WARNING: all shots in {exp_name} experiment collapsed to "
                        + str(counts) + ". Skipping it.")
                    continue
                z1r_set.append((counts['0'] - counts['1']) / self.shots)

            expectations.append(z1r_set)

        return expectations

    @timeit
    def retrieve(self, job_set_id, provider):
        """Retrieve job set results."""

        job_manager = IBMQJobManager()

        print("\n   ... retrieving job set\n")

        job_set = job_manager.retrieve_job_set(job_set_id, provider)
        print(job_set.report())

        jobs = job_set.jobs()
        self._monitor_jobs(jobs)

        results = self._fetch_results(job_set)

        return results

    @timeit
    def _fetch_results(self, jobset):

        print("\n   ... fetching results")

        return jobset.results().combine_results()

    def _cancel(self, jobs):
        """Cancel jobs explicitly"""

        print("\n   ... cancelling all jobs")

        for job in jobs:
            if job is not None:
                job.cancel()

    def _monitor_jobs(self, jobs):

        print("\n   ... waiting for jobs to complete\n")

        job_idx = 0
        n_jobs = len(jobs)

        while True:

            if all([j.status() in JOB_FINAL_STATES for j in jobs]):
                break

            for job in jobs:
                if job.status() == JobStatus.RUNNING:
                    job_idx += 1
                    print(f"=> Monitoring {job_idx}/{n_jobs} job:")
                    job_monitor(job, interval=10)

            time.sleep(20)

    def _add_exp_suffix(self, name, indx):
        return name + "_exp_" + str(indx)
