"""The module provides error mitigation facilities."""

from typing import List

from qiskit.providers import BaseBackend
from qiskit.ignis.mitigation.measurement import (complete_meas_cal,
                                                 CompleteMeasFitter)

from .utilities import timeit


@timeit
def get_readout_filter(backend: BaseBackend, measured_qubits: List[int]):
    """Build filter for readout error mitigation"""

    print(
        "   ... building calibration matrix for readout error mitigation on qubits:",
        measured_qubits)

    meas_calib_circuits, state_labels = complete_meas_cal(
        qr=backend.configuration().n_qubits,
        qubit_list=measured_qubits,
        circlabel='mcal_')

    calib_results = backend.run(meas_calib_circuits, shots=8192).result()

    meas_filter = CompleteMeasFitter(calib_results,
                                     state_labels,
                                     circlabel='mcal_').filter

    return meas_filter
