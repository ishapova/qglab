#!/usr/bin/env python
"""The bootstrapping script for experiments with holographic teleportation"""

#import logging
#logging.getLogger('qiskit.providers.ibmq').setLevel(logging.DEBUG)

import os
import sys
import datetime
import argparse
import pickle
from functools import partial
from random import random, choice
import numpy as np
import matplotlib.pyplot as plt

from qiskit import Aer, IBMQ, transpile
from qiskit.providers.aer import AerSimulator

#from qiskit.aqua.utils.circuit_utils import summarize_circuits
#from qiskit.aqua.utils.measurement_error_mitigation import get_measured_qubits

from qiskit.tools.monitor.overview import backend_overview  #,backend_monitor
#import qiskit.quantum_info as qi

#local imports
from qglab import layouts
from qglab import protocol
from qglab.errors import get_readout_filter
from qglab.utilities import (WormholeRunner, find_measured_qubits,
                             tket_transpile, tabulate, get_tket_layouts,
                             LayoutAdapter, count_cx)

N = 3  # number of qubits per side
m = 1  # message size single qubit

# Simulation parameters
np.random.seed(1)  # seed to reproduce h samplings
b = np.pi / 4  # transverse X rotation
J = np.pi / 4  # ZZ rotation strength
g = np.pi / 5  # interaction coupling strength

n_steps = 3  # number of hamiltonian evaluations for time evolution

parser = argparse.ArgumentParser(
    "Main Parser",
    description='Helper script for benchmarking holographic teleportation.')

parser.add_argument(
    '--qpu',
    type=str,
    help='Specify quantum device name, or leave empty to choose interactively')

parser.add_argument(
    '--retrials',
    '-r',
    type=int,
    required=True,
    help='Number of identical experiments to run for a coupling strength')

parser.add_argument('--shots',
                    '-s',
                    type=int,
                    default=8192,
                    help='Number of shots pers experiment')

parser.add_argument('--points',
                    '-p',
                    type=int,
                    required=True,
                    help='Number of coupling strengths to measure over')

group = parser.add_mutually_exclusive_group()
group.add_argument(
    '--async-submit',
    '--async',
    action='store_true',
    default=False,
    help=
    'Submit jobs to an IBMQ device and exit. Use --continue to continue when jobs are finished.'
)
group.add_argument(
    '--resume',
    type=str,
    metavar='JOBSETID',
    help=
    'Job set id to fetch results of an asynchronously submitted session and wrap up with remaining simulations'
)

parser.add_argument(
    '--layout-method',
    choices=[
        'trivial', 'dense', 'noise_adaptive', 'sabre', 'graph_based', 'linear'
    ],
    type=str,
    default='noise_adaptive',
    help="Layout method used to embed circuit onto a quantum device."+\
        " Qiskit methods: 'trivial', 'dense', 'noise_adaptive', 'sabre'" +\
            "tket methods: 'noise_adaptive', 'graph_based', 'linear'")

parser.add_argument('-O',
                    '--optimization',
                    type=int,
                    choices=[0, 1, 2, 3],
                    default=3,
                    help='Specify optimization level for transpilation')

parser.add_argument(
    '--dry-run',
    '-n',
    action='store_true',
    help='Execute the script without performing quantum experiment')

parser.add_argument(
    '--layout',
    type=str,
    help=
    "Set a layout or select one interactively based on simulation of a few from layout dictionary. "
    +
    "Format: a list of physical qubits (e.g., 1,2,3) or 'select' keyword to choose interactively."
)

parser.add_argument(
    '--insert-message-with',
    type=str,
    choices=['reset', 'swap', 'transfer'],
    help=
    "Set method for message insertion. Default is 'reset' if supported by device, otherwise 'swap'."
)

parser.add_argument(
    '--toolkit',
    type=str,
    choices=['qiskit', 'tket'],
    default='qiskit',
    help=
    "Set toolkit for circuit transpilation and optimization. Default: 'qiskit'."
)

_args = parser.parse_args()

if _args.layout_method is not None:
    if _args.toolkit == 'tket':
        assert _args.layout_method in ['noise_adaptive', 'graph_based', 'linear'],\
                f"Layout method {_args.layout_method} is unsupported by tket"
    if _args.toolkit == 'qiskit':
        assert _args.layout_method in ['trivial', 'dense', 'noise_adaptive', 'sabre'],\
                f"Layout method {_args.layout_method} is unsupported by Qiskit"

# load credentials and set up provider
if not IBMQ.stored_account():
    print(
        "\nNo IBM Quantum credentials found.",
        "Save them with IBMQ.save_account(TOKEN, hub=HUB, group=GROUP, project=PROJECT)"+\
        " and try again.\n"
    )
    sys.exit(1)
provider = IBMQ.load_account()

# give an overview of backends and request to choose one
_qpu = _args.qpu
if _qpu is None:
    backend_overview()
    _qpu = input("Choose quantum device: ")

_known_devices = [b.name() for b in provider.backends(simulator=False)]
assert _qpu in _known_devices, "Requested device is unknown, stopping."
_operational_devices = [
    b.name() for b in provider.backends(simulator=False, operational=True)
]
if _qpu not in _operational_devices:
    print("WARNING: Requested device is nonoperational")

# set up quantum device
device_backend = provider.get_backend(_qpu)
start_run_calibration_time = device_backend.properties().last_update_date
# set up simulators
ideal_simulator = Aer.get_backend('aer_simulator')
device_simulator = AerSimulator.from_backend(device_backend)
if _qpu == 'ibmq_manhattan':
    # enforce method 'statevector' for ibmq_manhattan since for the latter method
    # 'automatic' chooses 'density_matrix' which in turn throws floating point exception
    device_simulator.set_options(method='statevector')

# set message insertion method
device_config = device_backend.configuration()
if _args.insert_message_with is None:
    if 'reset' in device_config.supported_instructions and device_config.multi_meas_enabled:
        insert_message_method = 'reset'
    else:  # swap
        insert_message_method = 'swap'
else:
    if _args.insert_message_with == 'reset':
        assert ('reset' in device_config.supported_instructions
            and device_config.multi_meas_enabled), \
            "Requested to insert message with 'reset' but this instruction is not " + \
                 f"supported by {device_backend}. Choose a different method."
    insert_message_method = _args.insert_message_with

# total number of qubits in the experiment
total_n_qubits = 2 * N if insert_message_method == 'reset' else 2 * N + 1

# number of identical experiments per coupling strength
n_retrials = _args.retrials
# number of shots per experiment
n_shots = _args.shots
# number of coupling strenght to measure over
n_g = _args.points

# Output folder and filenames
_conf_str = f"__CONF__qubits={total_n_qubits}_steps={n_steps}_qpu={_qpu}_experiments=" + \
    f"{n_shots}x{n_retrials}" + \
    f"_O={_args.optimization}_layout={_args.layout if _args.layout else 'auto'}" + \
        f"_toolkit={_args.toolkit}"

_foldername = (
    "async-launch-" if _args.async_submit else "results-"
) + datetime.datetime.now().strftime("M%m-D%d-%H:%M:%S") + _conf_str
if not os.path.exists(_foldername):
    os.mkdir(_foldername)
_transpiled_foldername = _foldername + "/transpiled_circuits"
if not os.path.exists(_transpiled_foldername):
    os.mkdir(_transpiled_foldername)
_qasm_foldername = _foldername + "/qasm"
if not os.path.exists(_qasm_foldername):
    os.mkdir(_qasm_foldername)
_res_filename = _foldername + "/Results" + _conf_str
_circuit_filename = _foldername + "/High-level_Circuit" + _conf_str
_layouts_filename = _foldername + "/Layouts" + _conf_str

# Prepare figure
fig = plt.figure(figsize=(10, 7))
ax = fig.gca()
ax.set_xlabel(r"$g/(2\pi)$")
ax.set_ylabel(r"$\langle Z_{1R}\rangle_g$")
ax.set_title("Holographic teleportation")
ax.grid(True, axis='y', linestyle='solid', color='whitesmoke')
ax.set_axisbelow(True)

# prepare circuit factory ########################################
#h = np.random.uniform(-.5, .5, N)  # Z rotation angles (phi)
h = np.array([0.0283397, 0.00519953, 0.0316079
              ])  # rotation angles that maximize teleportation amplitude

factory = protocol.TeleporterFactory(N, m, b, J, h, n_steps,
                                     insert_message_method)
###################################################################

# generate high-level circuits
g_vals = np.linspace(0, (N - 1) * np.pi / 2, n_g)
_circuits = [factory.circuit(g_v) for g_v in g_vals]

# dump high-level circuit
#print(summarize_circuits(_circuits))
_circuits[0].draw(output='mpl', filename=_circuit_filename, idle_wires=False)

# handle layout
_selected_layout = None
if _args.layout not in [None, 'select']:
    assert ',' in _args.layout, "Layout uses wrong delimeter. Please use comma instead."
    _selected_layout = LayoutAdapter([int(s) for s in _args.layout.split(',')],
                                     sdk=_args.toolkit)
    # assert len(
    #    _selected_layout
    # ) == total_n_qubits, f"Dimension of the selected layout {_selected_layout} is different" + \
    #    f" from the number of qubits involved in the experiment ({total_n_qubits})"
else:
    _selected_layout = _args.layout

# build the library of transpiled circuits
_transpiled_circuits_library = {}
_selected_transpiled_circuits = []
print("\nBuilding the library of transpiled circuits...")
if _selected_layout == 'select':
    if _args.toolkit == 'qiskit':
        layout_set = layouts.get_layouts(_qpu, total_n_qubits, sdk='qiskit')
    else:  #tket
        layout_set = get_tket_layouts(_circuits[1], _qpu, provider,
                                      _args.layout_method)

    for lt in layout_set:
        print("   ... ", lt)
        if _args.toolkit == 'qiskit':
            transp_circuits = []
            for _c in _circuits:
                temp_transpiled_circuit = [
                    transpile(_c,
                              device_backend,
                              initial_layout=lt.get(),
                              layout_method=_args.layout_method,
                              optimization_level=_args.optimization,
                              routing_method='stochastic') for _ in range(20)
                ]

                transp_circuits.append(
                    min(temp_transpiled_circuit,
                        key=lambda x: x.count_ops()['cx']))
        else:  # 'tket'

            transp_circuits = [
                transpile(  # unroll U* gates to device basis with qiskit (rebasing in tket is obsolete)
                    tket_transpile(circ, _qpu, provider, _args.layout_method,
                                   lt),
                    basis_gates=device_backend.configuration().basis_gates,
                    optimization_level=3) for circ in _circuits
            ]

        for c in transp_circuits:
            c.metadata = {"meas_qubits": find_measured_qubits(c)}

        _transpiled_circuits_library[lt] = transp_circuits
else:
    if _args.toolkit == 'qiskit':
        print(" figuring minimal CX count from 1000 stochastic transpilations")
        min_cx_count = min([
            transpile(_circuits[1],
                      device_backend,
                      initial_layout=_selected_layout,
                      layout_method=_args.layout_method,
                      optimization_level=_args.optimization,
                      routing_method='stochastic') for _ in range(1000)
        ],
                           key=lambda x: x.count_ops()['cx']).count_ops()['cx']
        print(f"  best CX count: {min_cx_count}, making it the target")

        for _c in _circuits:
            print(" transpiling ", _c.name)
            cx_count = min_cx_count + 1
            while cx_count > min_cx_count:
                tr_c = transpile(_c,
                                 device_backend,
                                 initial_layout=_selected_layout,
                                 layout_method=_args.layout_method,
                                 optimization_level=_args.optimization,
                                 routing_method='stochastic')
                cx_count = tr_c.count_ops()['cx']
            _selected_transpiled_circuits.append(tr_c)

    else:  # 'tket'
        _selected_transpiled_circuits = [
            transpile(  # unroll U* gates to device basis with qiskit (rebasing in tket is obsolete)
                tket_transpile(circ, _qpu, provider, _args.layout_method,
                               _selected_layout),
                basis_gates=device_backend.configuration().basis_gates,
                optimization_level=3) for circ in _circuits
        ]

    for c in _selected_transpiled_circuits:
        c.metadata = {"meas_qubits": find_measured_qubits(c)}

#print(summarize_circuits(_transpiled_circuits))

# prepare runner
runner = WormholeRunner(_circuits,
                        shots=n_shots,
                        retrials=n_retrials,
                        if_dry_run=_args.dry_run)

#######################################################################
# Simulate the initial layouts imported from layouts.py and let
# the user choose one for subsequent quantum experiment, if requested
#######################################################################
if _args.layout == 'select':
    fig_layouts = plt.figure(figsize=(10, 7))
    ax_layouts = fig_layouts.gca()
    ax_layouts.set_xlabel(r"$g/(2\pi)$")
    ax_layouts.set_ylabel(r"$\langle Z_{1R}\rangle_g$")
    ax_layouts.set_title(
        "Holographic teleportation\n" +
        f"[Qubits: 2x{N}+1, time steps: {n_steps}, QPU: {_qpu}, " +
        f"shots*retrials/coupling: {n_shots}*{n_retrials}]")

    print("\nProbing registered initial layouts on default noise model:")
    layout_vs_expectations = {}

    for lt in layout_set:
        print(f"   probing layout {lt}")

        noisy_results = runner.run(_transpiled_circuits_library[lt],
                                   device_simulator)

        fltr = partial(get_readout_filter, device_simulator)
        mitigated_expectations = runner.expectations(noisy_results, fltr)

        ax_layouts.errorbar(
            g_vals / (2 * np.pi),
            [np.mean(distr) for distr in mitigated_expectations],
            yerr=[np.std(distr) for distr in mitigated_expectations],
            fmt=choice(['<', '>', 'o', 'x', 'd', '*', 'v', '^', 's']),
            color=(random(), random(), random()),
            label=f"Mimick QPU, layout: {lt}")

        layout_vs_expectations[lt] = np.median(
            [np.mean(distr) for distr in mitigated_expectations])

        fig_layouts.savefig(_layouts_filename)

    ax_layouts.legend(prop={'size': 5}, framealpha=0.5, loc='upper right')
    fig_layouts.tight_layout()
    fig_layouts.savefig(_layouts_filename)

    # Summarize the above simulations
    rows = []
    for lt in sorted(layout_vs_expectations,
                     key=lambda x: layout_vs_expectations[x],
                     reverse=True):
        cnots_on_measured = [
            count_cx(_transpiled_circuits_library[lt][1], m_q) for m_q in
            find_measured_qubits(_transpiled_circuits_library[lt][1])
        ]
        cnot_counts = [
            t_c.count_ops()['cx'] for t_c in _transpiled_circuits_library[lt]
        ]
        depth_counts = [
            t_c.depth() for t_c in _transpiled_circuits_library[lt]
        ]

        sizes = [t_c.size() for t_c in _transpiled_circuits_library[lt]]

        rows.append([
            str(lt.idx),
            str(lt).replace(" ", ""), f"{layout_vs_expectations[lt]:.3f}",
            str(cnots_on_measured).replace(" ", ""),
            str(cnot_counts).replace(" ", ""),
            str(depth_counts).replace(" ", ""),
            str(sizes).replace(" ", "")
        ])

    tabulate(rows,
             headers=[
                 "#", "Layout", "<Z> median (\u2193)", "CNOTs@Meas",
                 "CNOTs per coupling", "Circuit depth per coupling",
                 "Circuit size per coupling"
             ])

    # can choose the best one automatically, but let's keep this under manual control
    lt_idx = int(input("\nChoose your layout #: "))
    for lt in layout_set:
        if lt.idx == lt_idx:
            _selected_layout = lt
            break
    assert _selected_layout is not None, f"Layout with index {lt_idx}"

    _selected_transpiled_circuits = _transpiled_circuits_library[
        _selected_layout]

# find all measure qubits indices
_measured_qubits = []
for c in _selected_transpiled_circuits:
    measured_in_c = c.metadata['meas_qubits']
    if measured_in_c not in _measured_qubits:
        _measured_qubits.append(measured_in_c)

print("\nMeasured qubits across all circuits:", _measured_qubits)

# dump transpiled circuit diagrams and QASM represenations
print("\nDumping transpiled circuits diagrams and QASM representations...")
for idx, circ in enumerate(_selected_transpiled_circuits):
    circ.draw(output='mpl',
              filename=_transpiled_foldername + "/transpiled_circ_" + str(idx),
              idle_wires=False)
    circ.qasm(filename=_qasm_foldername + "/transpiled_circ" + str(idx) +
              ".qasm")

# Plot circuit depth and counts as a function of coupling strength ################
print("\nAnalyzing transpiled circuits' composition for each point...")

depths = [c.depth() for c in _selected_transpiled_circuits]

ax_circuit_depth = ax.twinx()
circ_size = _selected_transpiled_circuits[0].size()
ax_circuit_depth.set_ylim(top=6 * circ_size)
ax_circuit_depth.set_ylabel("Gate count")
width = 0.005
delta = width / 2
ax_circuit_depth.bar(g_vals / (2 * np.pi) - delta,
                     depths,
                     width=width,
                     label="Circuit depth",
                     color='red',
                     alpha=0.4)

# collect all possible used gates
ops_dict = {}
for c in _selected_transpiled_circuits:
    for key in c.count_ops():
        if key not in ops_dict and key not in ['barrier', 'measure']:
            ops_dict[key] = []

# collect counts for all used gates and print out gate composition summary
circs_summary = [[
    f"{g:.2f}",
    c.depth(),
    c.size(),
    str(list(c.count_ops().items()))
] for g, c in zip(g_vals, _selected_transpiled_circuits)]
tabulate(circs_summary, ["Coupling", "Depth", "Size", "Gates"])

for c in _selected_transpiled_circuits:
    circ_ops = c.count_ops()
    for key in ops_dict:
        ops_dict[key].append(circ_ops[key] if key in circ_ops else 0)

keys = sorted(ops_dict.keys())
bottom_counts = [0 for _ in range(len(g_vals))]
tick_values = []
for key, clr in zip(keys,
                    ['blue', 'black', 'darkorange', 'purple', 'darkgreen']):
    ax_circuit_depth.bar(g_vals / (2 * np.pi) + delta,
                         ops_dict[key],
                         width=width,
                         bottom=bottom_counts,
                         label=f"Gate {key}",
                         color=clr,
                         alpha=0.4)
    bottom_counts = [x + y for x, y in zip(bottom_counts, ops_dict[key])]
    if key not in ['reset', 'x']:
        tick_values.append(max(bottom_counts))

ax_circuit_depth.set_yticks(tick_values)
ax_circuit_depth.legend(prop={'size': 9}, loc='lower right')

#======================================================================#
######################### Perform experiments ##########################
#======================================================================#

# Perform quantum experiment ######################################
print(f"\nSampling Z expectations from quantum experiments on {_qpu}...")

if _args.resume is None:
    q_raw_results = runner.run(_selected_transpiled_circuits,
                               device_backend,
                               async_submit=_args.async_submit)
else:
    q_raw_results = runner.retrieve(_args.resume, provider)

if _args.async_submit:
    print(
        "\n Asynchronous submission, exiting. Append --resume to the original" +\
             " command to resume the batch process if your IBMQ submission was successful.\n"
    )
    sys.exit(0)

end_run_calibration_time = provider.get_backend(
    _qpu).properties().last_update_date

if not _args.dry_run:
    q_raw_expect = runner.expectations(q_raw_results)

    if q_raw_expect:
        ax.errorbar(g_vals / (2 * np.pi),
                    [np.mean(distr) for distr in q_raw_expect],
                    yerr=[np.std(distr) for distr in q_raw_expect],
                    fmt="*",
                    label="Quantum experiment (raw)")
        # pickle the figure for possible future style adjustments
        with open(_foldername + '/' + "raw_IBMQ_expectations.pickled",
                  'wb') as f:
            pickle.dump(q_raw_expect, f)

    # mitigate readout error
    fltr = partial(get_readout_filter, device_simulator)
    q_mitig_expect = runner.expectations(q_raw_results, fltr)

    if q_mitig_expect:
        ax.errorbar(
            g_vals / (2 * np.pi), [np.mean(distr) for distr in q_mitig_expect],
            yerr=[np.std(distr) for distr in q_mitig_expect],
            fmt="*",
            label=
            f"Quantum experiment ({'[*]' if len(_measured_qubits)>1 else _measured_qubits} readout mitigated)"
        )
        with open(
                _foldername + '/' +
                "readout_mitigated_IBMQ_expectations.pickled", 'wb') as f:
            pickle.dump(q_mitig_expect, f)

fig.savefig(_res_filename)

# Plot analytic result #################################################
# n = N - m
# g_vals_more = np.linspace(0, (N - 1) * np.pi / 2, 3 * n_g)
# f_g_vals = [
#     np.cos(g / n)**(n) * (-np.cos(g / n)**(n) + np.cos(g)) / 2
#     for g in g_vals_more
# ]
# ax.plot(g_vals_more / (2 * np.pi), f_g_vals, label="Large-time asymptotics")
# fig.savefig(_res_filename)

# Execute and post-process for teleportation fidelity

### Ideal simulation ######################################
#'reset' instruction prevents exact calculation of the expectation
if insert_message_method != 'reset':
    print("\nSimulating ideal Z expectations exactly...")
    ax.scatter(g_vals / (2 * np.pi),
               runner.exact_expectation(ideal_simulator),
               marker=".",
               zorder=6,
               label="Exact ideal simulation")

print("\nSampling ideal Z expectations...")
ideal_transpiled_circuits = transpile(_circuits,
                                      ideal_simulator,
                                      optimization_level=_args.optimization)
ideal_results = runner.run(ideal_transpiled_circuits, ideal_simulator)
ideal_expect = runner.expectations(ideal_results)

if ideal_expect:
    ax.errorbar(g_vals / (2 * np.pi),
                [np.mean(distr) for distr in ideal_expect],
                yerr=[np.std(distr) for distr in ideal_expect],
                fmt='v',
                label="Shot-based ideal simulation")

fig.savefig(_res_filename)

### Simulate QPU ######################################

print(f"\nSampling Z expectations on mimicked {_qpu}...")

noisy_raw_results = runner.run(_selected_transpiled_circuits, device_simulator)
noisy_raw_expect = runner.expectations(noisy_raw_results)

if noisy_raw_expect:
    ax.errorbar(g_vals / (2 * np.pi),
                [np.mean(distr) for distr in noisy_raw_expect],
                yerr=[np.std(distr) for distr in noisy_raw_expect],
                fmt="<",
                label="Mimick QPU (raw)")

#mitigate readout errors
fltr = partial(get_readout_filter, device_simulator)
noisy_mitig_expect = runner.expectations(noisy_raw_results, fltr)

if noisy_mitig_expect:
    ax.errorbar(
        g_vals / (2 * np.pi), [np.mean(distr) for distr in noisy_mitig_expect],
        yerr=[np.std(distr) for distr in noisy_mitig_expect],
        fmt="<",
        label=
        f"Mimick QPU ({'[*]' if len(_measured_qubits)>1 else _measured_qubits} readout mitigated)"
    )

# add legends to the main figure and write it out
fig.tight_layout()
ax.legend(prop={'size': 9}, loc='center right')

box_str = '\n'.join((
    f"QPU: {_qpu} (v{device_backend.properties().backend_version})",
    f"Calibrated on: {start_run_calibration_time}",
    f"Used qubits: {_circuits[0].num_qubits}, time steps: {n_steps}",
    f"Initial qubit layout: {'automatic' if _args.layout is None else str(_selected_layout.layout_flat)}",
    f"Message inserted with: {insert_message_method}",
    f"Points: {n_g}, shots: {n_shots}, retrials: {n_retrials}",
    f"Toolkit: {_args.toolkit}"))
props = dict(boxstyle='round', facecolor='steelblue', alpha=0.5)
ax.text(0.01,
        0.5,
        box_str,
        transform=ax.transAxes,
        fontsize=9,
        horizontalalignment='left',
        verticalalignment='center',
        bbox=props)
fig.savefig(_res_filename)

# pickle the figure for possible future style adjustments
with open(_foldername + '/' + "results.figure.pickled", 'wb') as f:
    pickle.dump(fig, f)

print(f"\nWrote {_foldername}")

print("Done!\n")
