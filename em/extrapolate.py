#!/usr/bin/env python3

# Author: Miroslav Urbanek <urbanek@lbl.gov>

import argparse, json, numpy

parser = argparse.ArgumentParser(description='Extrapolate results.')
parser.add_argument('file', help='processed results')
args = parser.parse_args()

with open(args.file, 'rt') as f:
    metadata = json.load(f)

estimation = numpy.array(metadata['estimation'])
order = metadata['order']
root = metadata['root']
target = numpy.array(metadata['target'])
mitigation = numpy.array(metadata['mitigation'])

matrix = numpy.zeros((order + 1, order + 1))
for i in range(order + 1):
    for j in range(order + 1):
        matrix[i, j] = (2 * i + 1)**j

covector = numpy.linalg.inv(matrix)[0, :]

print(target[0, 0], target[0, 1], covector @ target[:, 0],
      numpy.sqrt(numpy.square(covector) @ numpy.square(target[:, 1])),
      mitigation[0, 0], mitigation[0, 1], covector @ mitigation[:, 0],
      numpy.sqrt(numpy.square(covector) @ numpy.square(mitigation[:, 1])))
