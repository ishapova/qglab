#!/usr/bin/env python3

# Author: Miroslav Urbanek <urbanek@lbl.gov>

import argparse, json, numpy

parser = argparse.ArgumentParser(description='Process results.')
parser.add_argument('file', help='unfolded metadata')
args = parser.parse_args()

with open(args.file) as f:
    metadata = json.load(f)

estimation = metadata['estimation']
order = metadata['order']
target = metadata['target']
mitigation = []

for i in range(order + 1):
    ez = 2 * numpy.array(estimation[i]) - 1
    tz = 2 * numpy.array(target[i]) - 1
    el = len(ez)
    tl = len(tz)
    em = ez.mean()
    tm = tz.mean()
    ev = ez.var(ddof=1) / el
    tv = tz.var(ddof=1) / tl
    mm = tm / em
    mv = (em**2 * tv + tm**2 * ev) / em**4

    estimation[i] = (em, numpy.sqrt(ev))
    target[i] = (tm, numpy.sqrt(tv))
    mitigation.append((mm, numpy.sqrt(mv)))

metadata['estimation'] = estimation
metadata['target'] = target
metadata['mitigation'] = mitigation
print(json.dumps(metadata))
