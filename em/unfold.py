#!/usr/bin/env python3

# Author: Miroslav Urbanek <urbanek@lbl.gov>

import argparse, json, numpy

parser = argparse.ArgumentParser(description='Unfold a result.')
parser.add_argument('file', help='result metadata')
args = parser.parse_args()

with open(args.file) as f:
    metadata = json.load(f)

instances = metadata['instances']
inverts = metadata['inverts']
order = metadata['order']
results = metadata['results']
shots = metadata['shots']
qubits = metadata['qubits']
width = len(str(instances))


def raw(r, e):
    return e / shots


def inverse(r, e):
    return numpy.linalg.pinv(r) @ e / shots


def bayesian(r, e):

    def unfold(r, e, v):
        r = r @ numpy.diag(v)
        r = numpy.diag(1 / numpy.sum(r, axis=1)) @ r
        w = e @ r
        return w / sum(w)

    epsilon = numpy.finfo(float).eps
    n = e.shape[0]
    v = numpy.ones(n) / n
    d = numpy.inf

    while (d > 256 * epsilon):
        w = unfold(r, e, v)
        d = sum(abs(w - v)) / 2
        v = w

    return w


# method = raw
# method = inverse
method = bayesian

r0 = (results[0] + results[1]) / (2 * shots)
r1 = (results[2] + results[3]) / (2 * shots)
readout = numpy.array([[r0, r1], [1 - r0, 1 - r1]])
unfolds = numpy.empty(2 * instances * (order + 1))
qubit = qubits[0]

l = 0
for i in range(instances):
    for j in range(order + 1):
        for k in [False, True]:
            t = 't' if k else 'e'
            n = f'i{i:0{width}d}-o{j}-{t}'
            c = results[l + 4]
            u = method(readout, numpy.array([c, shots - c]))[0]
            unfolds[l] = 1 - u if inverts[n][qubit] else u
            l += 1

estimation = []
target = []
for i in range(order + 1):
    estimation.append(unfolds[2 * i::2 * (order + 1)].tolist())
    target.append(unfolds[2 * i + 1::2 * (order + 1)].tolist())

del metadata['inverts']
del metadata['qubits']
del metadata['results']
del metadata['shots']
metadata['estimation'] = estimation
metadata['target'] = target
print(json.dumps(metadata))
