#!/usr/bin/env python3

# Author: Miroslav Urbanek <urbanek@lbl.gov>

import argparse, json, numpy, os, sys, qiskit
import optimize_1q_gates
import qiskit.circuit.equivalence_library

parser = argparse.ArgumentParser(description='Execute mitigation circuits.')
parser.add_argument('directory', help='mitigation circuits')
parser.add_argument('backend', help='backend name')
args = parser.parse_args()

directory = args.directory
name = args.backend
shots = 8192

with open(os.path.join(directory, 'metadata.json'), 'rt') as f:
    metadata = json.load(f)

instances = metadata['instances']
inverts = metadata['inverts']
order = metadata['order']
root = metadata['root']
width = len(str(instances))


def measured(circuit):
    s = set()
    for i, q, _ in circuit.data:
        if isinstance(i, qiskit.circuit.measure.Measure):
            s.update({circuit.qubits.index(j) for j in q})
    return s


def manager():
    b = ['u', 'cx']
    s = qiskit.circuit.equivalence_library.SessionEquivalenceLibrary
    m = qiskit.transpiler.PassManager()
    m.append([
        qiskit.transpiler.passes.UnrollCustomDefinitions(s, b),
        qiskit.transpiler.passes.BasisTranslator(s, b)
    ])
    m.append([
        optimize_1q_gates.Optimize1qGates(b),
        qiskit.transpiler.passes.Depth(),
        qiskit.transpiler.passes.FixedPoint('depth')
    ],
             do_while=lambda s: not s['depth_fixed_point'])
    return m


manager = manager()


def readout(size, qubit, gate):
    c = qiskit.QuantumCircuit(size, 1)

    if gate == 'i': c.u(0, 0, 0, qubit)
    elif gate == 'x': c.x(qubit)
    elif gate == 'y': c.y(qubit)
    elif gate == 'z': c.z(qubit)
    c.measure(qubit, 0)

    return manager.run(c)


size = 0
measures = set()
mitigations = []

for i in range(instances):
    for j in range(order + 1):
        for k in [False, True]:
            t = 't' if k else 'e'
            n = f'i{i:0{width}d}-o{j}-{t}.qasm'
            f = os.path.join(directory, n)
            c = qiskit.QuantumCircuit.from_qasm_file(f)
            if c.num_qubits > size:
                size = c.num_qubits
            measures.update(measured(c))
            mitigations.append(c)

circuits = []
qubits = sorted(measures)

for i in qubits:
    circuits.append(readout(size, i, 'i'))
    circuits.append(readout(size, i, 'z'))
    circuits.append(readout(size, i, 'x'))
    circuits.append(readout(size, i, 'y'))

circuits.extend(mitigations)

if not qiskit.IBMQ.stored_account():
    print(
        "\nNo IBM Quantum credentials found.",
        "Save them with IBMQ.save_account(TOKEN, hub=HUB, group=GROUP, project=PROJECT)"+\
        " and try again.\n"
    )
    sys.exit(1)

provider = qiskit.IBMQ.load_account()
backend = provider.get_backend(name)

job = qiskit.execute(circuits, backend, optimization_level=0, shots=shots)
# job = backend.run(circuits, shots=shots)

metadata['id'] = job.job_id()
metadata['backend'] = backend.name()
metadata['qubits'] = qubits
metadata['shots'] = shots
print(json.dumps(metadata))
