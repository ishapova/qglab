#!/usr/bin/env python3

# Author: Miroslav Urbanek <urbanek@lbl.gov>

import argparse, json, os, qiskit, random
import optimize_1q_gates
import qiskit.circuit.equivalence_library

parser = argparse.ArgumentParser(description='Generate mitigation circuits.')
parser.add_argument('file', help='QASM circuit')
parser.add_argument('--instances',
                    default=8,
                    type=int,
                    help='number of random instances')
parser.add_argument('--order', default=2, type=int, help='extrapolation order')
args = parser.parse_args()


def layers(dagcircuit):
    l = []
    m = []
    for i in dagcircuit.multigraph_layers():
        u = []
        r = []
        c = []
        for j in i:
            if (j.name == 'u3'): u.append(j)
            elif (j.name == 'reset'): r.append(j)
            elif (j.name == 'cx'): c.append(j)
            elif (j.name == 'measure'): m.append(j)
            elif (j.name == 'barrier'): pass
            elif (j.type == 'op'): raise ValueError('unexpected operator')
        if len(u): l.append({'type': 'u3', 'nodes': u})
        if len(r): l.append({'type': 'reset', 'nodes': r})
        if len(c): l.append({'type': 'cx', 'nodes': c})
    if len(m): l.append({'type': 'measure', 'nodes': m})

    return l


def qubits(dagcircuit):
    q = set()
    for i in dagcircuit.topological_op_nodes():
        if i.name != 'barrier':
            q.update(i.qargs)

    return list(q)


table = {
    ('i', 'i'): ('i', 'i'),
    ('i', 'x'): ('i', 'x'),
    ('i', 'y'): ('z', 'y'),
    ('i', 'z'): ('z', 'z'),
    ('x', 'i'): ('x', 'x'),
    ('x', 'x'): ('x', 'i'),
    ('x', 'y'): ('y', 'z'),
    ('x', 'z'): ('y', 'y'),
    ('y', 'i'): ('y', 'x'),
    ('y', 'x'): ('y', 'i'),
    ('y', 'y'): ('x', 'z'),
    ('y', 'z'): ('x', 'y'),
    ('z', 'i'): ('z', 'i'),
    ('z', 'x'): ('z', 'x'),
    ('z', 'y'): ('i', 'y'),
    ('z', 'z'): ('i', 'z')
}

igate = qiskit.circuit.library.UGate(0, 0, 0)
xgate = qiskit.circuit.library.XGate()
ygate = qiskit.circuit.library.YGate()
zgate = qiskit.circuit.library.ZGate()


def twirl(dagcircuit, qubits, gates=None):
    l = len(qubits)
    if gates is None:
        gates = random.choices(['i', 'x', 'y', 'z'], k=l)
    for i in range(l):
        g = gates[i]
        if g == 'i': dagcircuit.apply_operation_back(igate, [qubits[i]])
        elif g == 'x': dagcircuit.apply_operation_back(xgate, [qubits[i]])
        elif g == 'y': dagcircuit.apply_operation_back(ygate, [qubits[i]])
        elif g == 'z': dagcircuit.apply_operation_back(zgate, [qubits[i]])
    return gates


def generate(quantumcircuit, layers, target, order):
    q = quantumcircuit.qubits
    d = qiskit.converters.circuit_to_dag(quantumcircuit)
    a = qubits(d)
    b = qiskit.circuit.library.Barrier(len(a))
    e = qiskit.dagcircuit.DAGCircuit()
    e.add_qubits(q)
    e.add_clbits(quantumcircuit.clbits)
    for r in quantumcircuit.qregs:
        e.add_qreg(r)
    for r in quantumcircuit.cregs:
        e.add_creg(r)

    for l in layers(d):
        if l['type'] == 'cx':
            for _ in range(2 * order + 1):
                g = twirl(e, a)
                e.apply_operation_back(b, a)
                for n in l['nodes']:
                    i = a.index(n.qargs[0])
                    j = a.index(n.qargs[1])
                    (g[i], g[j]) = table[(g[i], g[j])]
                    e.apply_operation_back(n.op, n.qargs, n.cargs)
                e.apply_operation_back(b, a)
                twirl(e, a, g)
        elif target or not l['type'] == 'u3':
            if l['type'] == 'measure':
                h = twirl(e, a)
            if l['type'] == 'reset':
                e.apply_operation_back(b, a)
            for n in l['nodes']:
                e.apply_operation_back(n.op, n.qargs, n.cargs)
            if l['type'] == 'reset':
                e.apply_operation_back(b, a)

    i = []
    for j in range(len(q)):
        if q[j] in a:
            k = a.index(q[j])
            i.append(True if h[k] == 'x' or h[k] == 'y' else False)
        else:
            i.append(False)

    return e, i


def manager():
    b = ['u', 'cx']
    s = qiskit.circuit.equivalence_library.SessionEquivalenceLibrary
    m = qiskit.transpiler.PassManager()
    m.append([
        qiskit.transpiler.passes.UnrollCustomDefinitions(s, b),
        qiskit.transpiler.passes.BasisTranslator(s, b)
    ])
    m.append([
        optimize_1q_gates.Optimize1qGates(b),
        qiskit.transpiler.passes.Depth(),
        qiskit.transpiler.passes.FixedPoint('depth')
    ],
             do_while=lambda s: not s['depth_fixed_point'])
    return m


manager = manager()

original = qiskit.QuantumCircuit.from_qasm_file(args.file)
circuit = qiskit.transpile(original,
                           basis_gates=['u3', 'cx'],
                           optimization_level=0)

root, _ = os.path.splitext(args.file)

try:
    os.mkdir(f'{root}-mitigation')
except FileExistsError:
    pass

inverts = {}
width = len(str(args.instances))

for i in range(args.instances):
    for j in range(args.order + 1):
        for k in [False, True]:
            d, l = generate(circuit, layers, k, j)
            c = manager.run(qiskit.converters.dag_to_circuit(d))
            t = 't' if k else 'e'
            n = f'i{i:0{width}d}-o{j}-{t}'
            inverts[n] = l
            c.qasm(filename=f'{root}-mitigation/{n}.qasm')

metadata = {
    'instances': args.instances,
    'inverts': inverts,
    'order': args.order,
    'root': root
}

with open(f'{root}-mitigation/metadata.json', 'wt') as f:
    json.dump(metadata, f)
    f.write('\n')
