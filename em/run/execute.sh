#!/bin/sh

for i in *-mitigation; do
    echo $(date): "$i"
    ../execute.py "$i" ibmq_montreal > $(basename "$i" -mitigation)-execute.json
done
