#!/bin/sh

for i in *-process.json; do
    echo $(basename "$i" -process.json) $(../extrapolate.py "$i")
done > mitigation.dat
