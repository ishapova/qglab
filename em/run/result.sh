#!/bin/sh

for i in *-execute.json; do
    echo $(date): "$i"
    ../result.py "$i" > $(basename "$i" -execute.json)-result.json
done
