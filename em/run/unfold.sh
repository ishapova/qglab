#!/bin/sh

for i in *-result.json; do
    echo $(date): "$i"
    ../unfold.py "$i" > $(basename "$i" -result.json)-unfold.json
done
