#!/bin/sh

for i in *.qasm; do
    echo $(date): "$i"
    ../generate.py --instances 256 --order 0 "$i"
done
