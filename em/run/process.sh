#!/bin/sh

for i in *-unfold.json; do
    echo $(date): "$i"
    ../process.py "$i" > $(basename "$i" -unfold.json)-process.json
done
