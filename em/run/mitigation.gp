set terminal pngcairo font "FreeSans" size 800,494
set output "mitigation.png"

set encoding utf8
set format x "%.1f"
set format y "%.2f"
set minussign
set xzeroaxis
unset key

set xrange [-0.025:0.525]
set yrange [-0.4:0.05]

set xlabel "{/:Italic g}/(2{/:Italic π})"
set ylabel "⟨{/:Italic Z}_{1{/:Italic R}}⟩_{/:Italic g}"

plot "mitigation.dat" u ($0/30):6:7 w e pt 7 ps 0.25
