#!/usr/bin/env python3

# Author: Miroslav Urbanek <urbanek@lbl.gov>

import argparse, json, qiskit, sys

parser = argparse.ArgumentParser(description='Retrieve a result.')
parser.add_argument('file', help='execution metadata')
args = parser.parse_args()

with open(args.file, 'rt') as f:
    metadata = json.load(f)

if not qiskit.IBMQ.stored_account():
    print(
        "\nNo IBM Quantum credentials found.",
        "Save them with IBMQ.save_account(TOKEN, hub=HUB, group=GROUP, project=PROJECT)"+\
        " and try again.\n"
    )
    sys.exit(1)

provider = qiskit.IBMQ.load_account()
backend = provider.get_backend(metadata['backend'])

job = backend.retrieve_job(metadata['id'])
result = job.result()

results = []
for i in range(len(result.results)):
    results.append(result.get_counts(i).get('0', 0))

metadata['results'] = results
print(json.dumps(metadata))
